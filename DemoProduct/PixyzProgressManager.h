#pragma once

public delegate void PixyzProgressDelegate(int);

public ref class PixyzProgressManagerBase {
public:
	static event PixyzProgressDelegate^ changed;
};

ref class PixyzProgressManager : public PixyzProgressManagerBase
{
public:
	void raiseChanged(int p) {
		PixyzProgressManagerBase^ prg = gcnew PixyzProgressManagerBase;
		prg->changed(p);
	}
};
