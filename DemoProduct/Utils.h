#pragma once

#include <msclr\marshal_cppstd.h>
#include <iostream>
#include <windows.h>
#include <vector>

class Utils {
public:
	static std::string managedToUnmanaged(System::String^ managed) {
		return msclr::interop::marshal_as<std::string>(managed);
	}
	static System::String^ unmanagedToManaged(std::string unmanaged) {
		return msclr::interop::marshal_as<System::String^>(unmanaged);
	}

	static std::string workingDir() {
		wchar_t buffer[MAX_PATH];
		GetModuleFileName(NULL, buffer, MAX_PATH);
		std::wstring ws = std::wstring(buffer);
		std::string::size_type pos = std::string(ws.begin(), ws.end()).find_last_of("\\/");
		return std::string(ws.begin(), ws.end()).substr(0, pos);
	}

	static void runExe(std::string path, std::vector<std::string> args) {
		std::wstring path_wstr(path.begin(), path.end());
		std::wstring w_args = L"";
		for (int i = 0; i < args.size(); i++) {
			w_args += std::wstring(args[i].begin(), args[i].end()) + L" ";
		}
		// start the program up
		ShellExecute(NULL, L"open", path_wstr.c_str(), w_args.c_str(), NULL, SW_SHOWDEFAULT);
	}
};