#pragma once

namespace DemoProduct {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;
	using namespace System::Threading;



	/// <summary>
	/// Summary for MainWindow
	/// </summary>
	public ref class MainWindow : public System::Windows::Forms::Form
	{
	private: 
		enum class Action {
			Import = 0,
			Export = 1,
			Process = 2
		};

		System::Collections::Queue^ actions = gcnew System::Collections::Queue();
		PixyzSample& pixyz = PixyzSample::Instance();
		Thread^ backgroundThread;

		System::Windows::Forms::ToolStripMenuItem^  optimizeToolStripMenuItem;
		System::Windows::Forms::ToolStripMenuItem^  runGenericProcessToolStripMenuItem;
		System::Windows::Forms::Button^  resetButton;
		System::Windows::Forms::Label^  label1;
		System::Windows::Forms::Label^  label2;
		System::Windows::Forms::Label^  trianglesValueLabel;
		System::Windows::Forms::Label^  partsValueLabel;
		System::Windows::Forms::MenuStrip^  menuStrip1;
		System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
		System::Windows::Forms::ToolStripMenuItem^  importToolStripMenuItem;
		System::Windows::Forms::ToolStripMenuItem^  exportToolStripMenuItem;
		System::Windows::Forms::ProgressBar^  progressBar;
		System::Windows::Forms::Label^  progressLabel;

		System::Void import_Click(System::Object^ sender, System::EventArgs^ e);
		void showError(String^ message);
		void updateValues();
		System::Void export_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void resetButton_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void runProcess(System::Object^  sender, System::EventArgs^  e);
		OpenFileDialog^ openFileDialog;
		SaveFileDialog^ saveFileDialog;
		void runBackgroundThread();

	public:
		MainWindow()
		{
			backgroundThread = gcnew Thread(gcnew ThreadStart(this, &MainWindow::runBackgroundThread));
			backgroundThread->Start();
			InitializeComponent();
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &MainWindow::closing);
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainWindow()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MainWindow::typeid));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->trianglesValueLabel = (gcnew System::Windows::Forms::Label());
			this->partsValueLabel = (gcnew System::Windows::Forms::Label());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->importToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exportToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->optimizeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->runGenericProcessToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->progressBar = (gcnew System::Windows::Forms::ProgressBar());
			this->progressLabel = (gcnew System::Windows::Forms::Label());
			this->resetButton = (gcnew System::Windows::Forms::Button());
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei UI", 10.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(12, 42);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(95, 25);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Triangles";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei UI", 10.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(14, 70);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(58, 25);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Parts";
			// 
			// trianglesValueLabel
			// 
			this->trianglesValueLabel->AutoSize = true;
			this->trianglesValueLabel->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei UI", 10.8F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->trianglesValueLabel->Location = System::Drawing::Point(113, 42);
			this->trianglesValueLabel->Name = L"trianglesValueLabel";
			this->trianglesValueLabel->Size = System::Drawing::Size(23, 25);
			this->trianglesValueLabel->TabIndex = 3;
			this->trianglesValueLabel->Text = L"0";
			// 
			// partsValueLabel
			// 
			this->partsValueLabel->AutoSize = true;
			this->partsValueLabel->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei UI", 10.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->partsValueLabel->Location = System::Drawing::Point(113, 70);
			this->partsValueLabel->Name = L"partsValueLabel";
			this->partsValueLabel->Size = System::Drawing::Size(23, 25);
			this->partsValueLabel->TabIndex = 4;
			this->partsValueLabel->Text = L"0";
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->fileToolStripMenuItem,
					this->optimizeToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
			this->menuStrip1->Size = System::Drawing::Size(437, 28);
			this->menuStrip1->TabIndex = 5;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->importToolStripMenuItem,
					this->exportToolStripMenuItem
			});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(44, 24);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// importToolStripMenuItem
			// 
			this->importToolStripMenuItem->Name = L"importToolStripMenuItem";
			this->importToolStripMenuItem->Size = System::Drawing::Size(216, 26);
			this->importToolStripMenuItem->Text = L"Import";
			this->importToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWindow::import_Click);
			// 
			// exportToolStripMenuItem
			// 
			this->exportToolStripMenuItem->Name = L"exportToolStripMenuItem";
			this->exportToolStripMenuItem->Size = System::Drawing::Size(216, 26);
			this->exportToolStripMenuItem->Text = L"Export";
			this->exportToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWindow::export_Click);
			// 
			// optimizeToolStripMenuItem
			// 
			this->optimizeToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->runGenericProcessToolStripMenuItem });
			this->optimizeToolStripMenuItem->Name = L"optimizeToolStripMenuItem";
			this->optimizeToolStripMenuItem->Size = System::Drawing::Size(82, 24);
			this->optimizeToolStripMenuItem->Text = L"Optimize";
			// 
			// runGenericProcessToolStripMenuItem
			// 
			this->runGenericProcessToolStripMenuItem->Name = L"runGenericProcessToolStripMenuItem";
			this->runGenericProcessToolStripMenuItem->Size = System::Drawing::Size(216, 26);
			this->runGenericProcessToolStripMenuItem->Text = L"Run Generic Process";
			this->runGenericProcessToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWindow::runProcess);
			// 
			// progressBar
			// 
			this->progressBar->BackColor = System::Drawing::Color::Gray;
			this->progressBar->Location = System::Drawing::Point(0, 111);
			this->progressBar->Name = L"progressBar";
			this->progressBar->Size = System::Drawing::Size(437, 33);
			this->progressBar->TabIndex = 6;
			// 
			// progressLabel
			// 
			this->progressLabel->AutoSize = true;
			this->progressLabel->BackColor = System::Drawing::Color::Transparent;
			this->progressLabel->Location = System::Drawing::Point(198, 91);
			this->progressLabel->Name = L"progressLabel";
			this->progressLabel->Size = System::Drawing::Size(12, 17);
			this->progressLabel->TabIndex = 7;
			this->progressLabel->Text = L" ";
			// 
			// resetButton
			// 
			this->resetButton->Location = System::Drawing::Point(357, 44);
			this->resetButton->Name = L"resetButton";
			this->resetButton->Size = System::Drawing::Size(64, 48);
			this->resetButton->TabIndex = 8;
			this->resetButton->Text = L"Reset";
			this->resetButton->UseVisualStyleBackColor = true;
			this->resetButton->Click += gcnew System::EventHandler(this, &MainWindow::resetButton_Click);
			// 
			// MainWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ControlLight;
			this->ClientSize = System::Drawing::Size(437, 144);
			this->Controls->Add(this->resetButton);
			this->Controls->Add(this->progressLabel);
			this->Controls->Add(this->progressBar);
			this->Controls->Add(this->partsValueLabel);
			this->Controls->Add(this->trianglesValueLabel);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->menuStrip1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"MainWindow";
			this->Text = L"Pixyz Sample";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


	private: void closing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
	{
		backgroundThread->Abort();
	}

	private: 
		delegate void UpdateProgressBarDelegate(int value);
		void updateProgressBar(int value) {
			ISynchronizeInvoke^ i = this;

			if (i->InvokeRequired)
			{
				UpdateProgressBarDelegate^ tempDelegate =
					gcnew UpdateProgressBarDelegate(this, &MainWindow::updateProgressBar);
				cli::array<System::Object^>^ args = gcnew cli::array<System::Object^>(1);
				args[0] = value;
				i->BeginInvoke(tempDelegate, args);
				return;
			}
			progressBar->Style = ProgressBarStyle::Continuous;
			progressBar->MarqueeAnimationSpeed = 0;
			progressLabel->Text = System::Convert::ToString(value);
			if (value >= 0) {
				progressBar->Value = value % 100;
			}
			if (value == 99) {
				progressBar->Value = 100;
				progressLabel->Text = "100";
			}
			if (value == 100 || value == 0) {
				progressLabel->Text = "";
			}
		}

	private: 
		delegate void ShowErrorDelegate(String^ value);
		void showErrorDelegate(System::String^ value) {
			ISynchronizeInvoke^ i = this;

			if (i->InvokeRequired)
			{
				ShowErrorDelegate^ tempDelegate =
					gcnew ShowErrorDelegate(this, &MainWindow::showErrorDelegate);
				cli::array<System::Object^>^ args = gcnew cli::array<System::Object^>(1);
				args[0] = value;
				i->BeginInvoke(tempDelegate, args);
				return;
			}
			this->showError(value);
		}
	};
}


