#pragma once

#include "FullSDKTypes.h"
#include "FullSDKInterface.h"
#include "PiXYZIO/interface/IOInterface.h"
#include "PixyzProgressManager.h"
#include "gcroot.h"

class PixyzSample
{
public:
	static PixyzSample& Instance();
	std::string getImportFormatsFilters();
	std::string getExportFormatsFilters();
	void importFile(std::string filePath);
	void exportFile(std::string filePath);
	void process();
	int getTriangleCount();
	int getPartCount();
	void addProgressChangedCallback();
	void initSDK();
	void resetScene();
	gcroot<PixyzProgressManager^> progress;
private:
	const PiXYZ::CoreI::String productKey = "DemoProduct";
	const PiXYZ::CoreI::String validationKey = "2048830f5f5fa159680dcba5fa4df3fcf22f5e302772225e480d084aa02afba213c36aee";
	const std::string licensePath = "\\license.lic";
	static PixyzSample instance;
	PixyzSample() { }
	~PixyzSample() { }
	double getSize(PiXYZ::SceneI::OccurrenceList root);
	std::string formatListToFilter(PiXYZ::IOI::FormatList formatList);
	static void progressChangedCallback(void*, PiXYZ::CoreI::Int value);
	void finishInstall();
};