#include "PixyzSample.h" // include before Windows files
#include "MainWindow.h"
#include "Utils.h"


using namespace System;
using namespace System::Windows::Forms;

[STAThread]
int main() {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	DemoProduct::MainWindow form;
	Application::Run(%form);
}

namespace DemoProduct {

	void MainWindow::runBackgroundThread() {
		// We opt here for an endless thread waiting to process an actions queue. It is not necessary because Pixyz SDK is thread-safe
		// We do it here because we want to get the values from Pixyz progress changed callback.
		try {
			pixyz.initSDK();
			pixyz.progress->changed += gcnew PixyzProgressDelegate(this, &MainWindow::updateProgressBar);
			pixyz.addProgressChangedCallback();
		}
		catch (const std::runtime_error ex) {
			showErrorDelegate(Utils::unmanagedToManaged(ex.what()));
			return;
		}

		while (true) {
			if (actions->Count == 0) continue;
			Action action = (Action)actions->Dequeue();
			switch (action) {
				case Action::Import:
					try {
						pixyz.importFile(Utils::managedToUnmanaged(openFileDialog->FileName));
						updateValues();
						updateProgressBar(0);
					}
					catch (const std::runtime_error ex) {
						showError(Utils::unmanagedToManaged(ex.what()));
					}
					break;
				case Action::Export:
					try {
						pixyz.exportFile(Utils::managedToUnmanaged(saveFileDialog->FileName));
						updateProgressBar(0);
					}
					catch (const std::runtime_error ex) {
						showError(Utils::unmanagedToManaged(ex.what()));
					}
					break;
				case Action::Process:
					try {
						pixyz.process();
						updateValues();
						updateProgressBar(0);
					}
					catch (const std::runtime_error ex) {
						showError(Utils::unmanagedToManaged(ex.what()));
					}
					break;
				default:
					break;
			}
		}
	}

	void MainWindow::updateValues() {
		trianglesValueLabel->Text = System::Convert::ToString(pixyz.getTriangleCount());
		partsValueLabel->Text = System::Convert::ToString(pixyz.getPartCount());
	}

	System::Void MainWindow::import_Click(System::Object^ sender, System::EventArgs^ e) {
		openFileDialog = gcnew OpenFileDialog();
		try {
			openFileDialog->Filter = gcnew String(pixyz.getImportFormatsFilters().c_str());
		}
		catch (const std::runtime_error ex) {
			showError(Utils::unmanagedToManaged(ex.what()));
			return;
		}
		if (openFileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
			progressBar->Style = ProgressBarStyle::Marquee;
			progressBar->MarqueeAnimationSpeed = 30;
			actions->Enqueue(Action::Import);
		}
		delete openFileDialog;
	}


	System::Void MainWindow::export_Click(System::Object^  sender, System::EventArgs^  e) {
		saveFileDialog = gcnew SaveFileDialog();
		try {
			saveFileDialog->Filter = gcnew String(pixyz.getExportFormatsFilters().c_str());
		}
		catch (const std::runtime_error ex) {
			showError(Utils::unmanagedToManaged(ex.what()));
			return;
		}
		if (saveFileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
			progressBar->Style = ProgressBarStyle::Marquee;
			progressBar->MarqueeAnimationSpeed = 30;
			actions->Enqueue(Action::Export);
		}
		delete saveFileDialog;
	}

	System::Void MainWindow::resetButton_Click(System::Object^  sender, System::EventArgs^  e) {
		try {
			pixyz.resetScene();
			updateValues();
		}
		catch (const std::runtime_error ex) {
			showError(Utils::unmanagedToManaged(ex.what()));
		}
	}

	System::Void MainWindow::runProcess(System::Object^  sender, System::EventArgs^  e) {
		progressBar->Style = ProgressBarStyle::Marquee;
		progressBar->MarqueeAnimationSpeed = 30;
		actions->Enqueue(Action::Process);
	}

	void MainWindow::showError(String^ message)
	{
		String^ caption = "Error";
		MessageBoxButtons buttons = MessageBoxButtons::OK;
		System::Windows::Forms::DialogResult result;

		// Displays the MessageBox.
		result = MessageBox::Show(this, message, caption, buttons);
	}
}


