#include "PixyzSample.h"
#include "PiXYZCore/interface/CoreInterface.h"
#include "PiXYZAlgo/interface/AlgoInterface.h"
#include "PiXYZScene/interface/SceneInterface.h"
#include "PiXYZView/interface/ViewInterface.h"
#include "PiXYZGeom/interface/GeomInterface.h"

#include "Utils.h"
#include <math.h>  
#include <fstream>


PixyzSample PixyzSample::instance = PixyzSample();

PixyzSample& PixyzSample::Instance()
{
	return instance;
}

void PixyzSample::initSDK() {
	try {
		// Initialize Pixyz sdk with the content of the .lic file
		std::ifstream licenseStream = std::ifstream(Utils::workingDir() + licensePath);
		std::string licenseContent((std::istreambuf_iterator<char>(licenseStream)), (std::istreambuf_iterator<char>()));
		PiXYZ::FullSDKI::FullSDKInterface::initialize(productKey, validationKey, licenseContent);
	} catch (PiXYZ::CoreI::Exception ex){
		// Check if PiXYZFinishInstall.exe has never been called before
		if (std::string(ex.getDescription()).find("The system date has been changed") != std::string::npos) {
			finishInstall();
			return;
		}
		throw std::runtime_error(std::string(ex.getDescription()));
	}
}

void PixyzSample::finishInstall() {
	std::string path = Utils::workingDir() + "\\PiXYZFinishInstall.exe";
	std::vector<std::string> args;
	args.push_back(this->productKey);
	Utils::runExe(path, args);
}

std::string PixyzSample::getImportFormatsFilters() {
	try {
		return PixyzSample::formatListToFilter(PiXYZ::IOI::IOInterface::getImportFormats());
	}
	catch (PiXYZ::CoreI::Exception ex) {
		throw std::runtime_error(std::string(ex.getDescription()));
	}
}

std::string PixyzSample::getExportFormatsFilters() {
	try {
		return PixyzSample::formatListToFilter(PiXYZ::IOI::IOInterface::getExportFormats());
	}
	catch (PiXYZ::CoreI::Exception ex) {
		throw std::runtime_error(std::string(ex.getDescription()));
	}
}

std::string PixyzSample::formatListToFilter(PiXYZ::IOI::FormatList formatList) {
	std::string allFiles = "All Pixyz Files (";
	std::string allFilesFilter = "";
	std::string filters = "";
	for (int i = 0; i < formatList.size(); i++)
	{
		PiXYZ::IOI::Format format = formatList[i];
		std::string formatValue = std::string(format.name) + "(";
		std::string formatFilter = "";
		for (int j = 0; j < format.extensions.size(); j++) {
			std::string filter = std::string(format.extensions[j]);
			allFiles += filter + ";";
			allFilesFilter += filter + ";";
			formatFilter += filter + ";";
		}
		formatValue += formatFilter + ")|" + formatFilter;
		filters += formatValue;
		if (i != formatList.size() - 1) filters += "|";
	}
	std::string result = allFiles + ")|" + allFilesFilter + "|" + filters;
	return result;
}

void PixyzSample::importFile(std::string filePath) {
	try {
		PiXYZ::IOI::IOInterface::importScene(PiXYZ::IOI::ImportFilePath(filePath));
	}
	catch (PiXYZ::CoreI::Exception ex) {
		throw std::runtime_error(std::string(ex.getDescription()));
	}
}

void PixyzSample::exportFile(std::string filePath) {
	try {
		PiXYZ::IOI::IOInterface::exportScene(PiXYZ::CoreI::OutputFilePath(filePath));
	}
	catch (PiXYZ::CoreI::Exception ex) {
		throw std::runtime_error(std::string(ex.getDescription()));
	}
}

int PixyzSample::getTriangleCount() {
	try {
		auto occurrence = PiXYZ::SceneI::SceneInterface::getRoot();
		auto occurrenceList = PiXYZ::SceneI::OccurrenceList(1, occurrence);
		PiXYZ::CoreI::Int value = PiXYZ::SceneI::SceneInterface::getPolygonCount(occurrenceList, true);
		return int(value);
	}
	catch (PiXYZ::CoreI::Exception ex) {
		throw std::runtime_error(std::string(ex.getDescription()));
	}
}

int PixyzSample::getPartCount() {
	try {
		auto parts = PiXYZ::SceneI::SceneInterface::getPartOccurrences();
		return int(parts.size());
	}
	catch (PiXYZ::CoreI::Exception ex) {
		throw std::runtime_error(std::string(ex.getDescription()));
	}
}

double PixyzSample::getSize(PiXYZ::SceneI::OccurrenceList root) {
	try {
		auto aabb = PiXYZ::SceneI::SceneInterface::getAABB(root);
		auto sub = PiXYZ::GeomI::Point3(aabb.high.x - aabb.low.x, aabb.high.y - aabb.low.y, aabb.high.z - aabb.low.z);
		auto dot = sub.x * sub.x + sub.y * sub.y + sub.z * sub.z;
		return sqrt(dot);
	}
	catch (PiXYZ::CoreI::Exception ex) {
		throw std::runtime_error(std::string(ex.getDescription()));
	}
}

void PixyzSample::process() {
	try {
		auto occurrence = PiXYZ::SceneI::SceneInterface::getRoot();
		auto root = PiXYZ::SceneI::OccurrenceList(1, occurrence);

		// Adapt tolerance for very small models
		double size = getSize(root);
		double baseTolerance = 0.1;
		if (size < 1000) baseTolerance = size / 10000;

		// Generic optimization process

		// Repair mesh is a must-do before any other functions is called to ensure better results.
		PiXYZ::AlgoI::AlgoInterface::repairMesh(root, baseTolerance, true, false);

		// Create normal attributes on tessellations in case there are not (completely black models)
		PiXYZ::AlgoI::AlgoInterface::createNormals(root, -1);

		// Decimate before Tessellate so Pixyz tessellation is not affected by decimation
		PiXYZ::AlgoI::AlgoInterface::decimate(root, 1, -1, 8, -1, false);
		
		// Repair CAD shapes, assemble faces, remove duplicated faces, optimize loops and repair topology (must do before tessellation)
		PiXYZ::AlgoI::AlgoInterface::repairCAD(root, baseTolerance);

		// Create a tessellated representation from a CAD representation (in case there is one)
		PiXYZ::AlgoI::AlgoInterface::tessellate(root, baseTolerance*2, -1, -1, true, PiXYZ::AlgoI::UVGenerationMode::NoUV, 0, (0.0), false, false, false, true);
		
		// Delete unnecessary information (usage dependant)
		PiXYZ::AlgoI::AlgoInterface::deletePatches(root, true);
		PiXYZ::AlgoI::AlgoInterface::deleteLines(root);
		PiXYZ::AlgoI::AlgoInterface::deleteFreeVertices(root);
	}
	catch (PiXYZ::CoreI::Exception ex) {
		throw std::runtime_error(std::string(ex.getDescription()));
	}
}

void PixyzSample::addProgressChangedCallback() {
	PiXYZ::CoreI::CoreInterface::addProgressChangedCallback(PixyzSample::progressChangedCallback);
}

void PixyzSample::progressChangedCallback(void *, PiXYZ::CoreI::Int value) {
	Instance().progress->raiseChanged(value);
}

void PixyzSample::resetScene() {
	try {
		PiXYZ::CoreI::CoreInterface::resetSession();
	}
	catch (PiXYZ::CoreI::Exception ex) {
		throw std::runtime_error(std::string(ex.getDescription()));
	}
}

