# full-sdk-sample

The full-sdk-sample is a Visual Studio C++/CLI project illustrating Pixyz C++ Full SDK integration.

`PixyzSample.cpp` shows how to use Pixyz API to initialize the SDK and process files.

# Getting Started

## Required Software

![](documentation/puce-general-64x64.png)|
:-:|
Pixyz Full SDK |

## Set Up
* Install `PiXYZFullSDK-xxxx.x.x-win64.exe`
* Open Visual Studio solution and check that all referencies are well set (additional dependencies, additional include directories...)
* Add or reference the content of `PiXYZFullSDK/bin/` in your project
* Get a license.lic file from the Pixyz team and paste it at `full-sdk-sample/DemoProduct/`
* Build and run